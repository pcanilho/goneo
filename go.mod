module gitlab.com/pcanilho/goneo

go 1.16

require (
	github.com/neo4j/neo4j-go-driver/v4 v4.3.4
	github.com/stretchr/testify v1.7.0
)
