![logo](docs/imgs/logo.png)

A library that aims at implementing a [Neo4J](https://neo4j.com/) [ORM](shorturl.at/pswFH) system using `Golang`.

## Milestones

- [x] `ORM` reflector that consumes arbitrary structs. 
- [x] CRUD controller for `Neo4J` engines.
- [x] Custom `tags` to allow overriding of identifiers and library feature selection.
- [x] Fully automated `CI` testing.
- [x] Full suite of `BDD` tests.

## How to use

### Allowed tags

* `omitempty`: The field will be ignored when found to be `empty`.
* `<identifier>`: The identifier to be used instead of the `field` name.
* `-`: The field will be ignored by the library.

### Define the objects

```go
type Person struct {
	URI      string `goneo:"-"`
	Name     string `goneo:"name"`
	Location string `goneo:",omitempty"`
}

type Council struct {
	Name string `goneo:"name"`
}
```

### Initialise the library

```go
// Initialise connection
addr, username, password := os.Getenv("GONEO_ADDRESS"), os.Getenv("GONEO_USERNAME"), os.Getenv("GONEO_PASSWORD")
driverInstance := goneo.NewNeo4JDriver(addr, 
	goneo.WithBasicAuth(username, password),    // Basic-Auth 
	goneo.WithUniqueNodes())                    // Unique nodes, i.e. Merge vs Create
if err := driverInstance.Connect(); err != nil {
    log.Fatalln(err)
}

// Initialise controllers
controllerInstance = goneo.NewNeo4JControllerAttached(driverInstance)
```

### Create the objects `Person` & `Council`

```go
p1 := &Person{
	Name: "person-1"
}

c1 := &Council{
	Name: "council-1"
}

// Create 
if err := controllerInstance.Create(p1); err != nil {
	return err
}

if err := controllerInstance.Create(c1); err != nil {
	return err
}
```

### List the objects of type `Person`

```go
var records []Person
if err := controllerInstance.List(&records); err != nil {
	return err
}
log.Println(records)
```

### Find objects of type `Person`

```go
var records []Person
if err := controllerInstance.Find(p1, &records); err != nil {
	return err
}
```

### Link the a `Person` to a `Council`

```go
if err := controllerInstance.Link(p1, c1, "Member"); err != nil {
	return err
}
```

### Delete all created objects

```go
if err := controllerInstance.Delete(p1); err != nil {
	return err
}
if err := controllerInstance.Delete(c1); err != nil {
	return err
}
```

### Get the leaf `Person` nodes connected to the supplied `Council` (regardless of nested links)
```go
var records []Person
if err := controllerInstance.GetLeafs(c1, &records, "Member"); err != nil {
	return err
}
```

## Acknowledgements
Thank you David [github](https://github.com/davidmogar) for your awesome avatar & logo contributions to this project!  

## Use-cases

### `User->Role->Role`

![use-case-1](docs/imgs/use_case_1.png)
