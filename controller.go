package goneo

type Controller interface {
	Attach(*neo4JDriver)
	Create(...interface{}) ([]interface{}, error)
	List(interface{}) error
	Find(interface{}, interface{}) error
	Delete(...interface{}) error
	Link(interface{}, string, ...interface{}) error
	GetLeafs(interface{}, interface{}, string) error
}
