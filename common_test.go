package goneo

import (
	"log"
	"os"
)

type User struct {
	Name  string `goneo:"name"`
	Email string `goneo:"email"`
}

type Team struct {
	Name string `goneo:"name"`
}

type Department struct {
	Name string `goneo:"name"`
}

var (
	user1, user2, user3, user4            *User
	team1, team2, team3                   *Team
	department1, department2, department3 *Department
)

var (
	driverInstance     *neo4JDriver
	controllerInstance Controller
)

func init() {
	// Initialise connection
	driverInstance = NewNeo4JDriver(os.Getenv("GONEO_ADDRESS"),
		WithBasicAuth(os.Getenv("GONEO_USERNAME"), os.Getenv("GONEO_PASSWORD")))
	if err := driverInstance.Connect(); err != nil {
		log.Fatalln(err)
	}

	// Initialise controllers
	controllerInstance = NewNeo4JControllerAttached(driverInstance)
}
