package goneo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestORMIdentifier(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected string
	}{
		{in: User{}, expected: "user"},
		{in: Team{}, expected: "team"},
		{in: Department{}, expected: "department"},

		{in: new(User), expected: "user"},
		{in: new(Team), expected: "team"},
		{in: new(Department), expected: "department"},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-identifier-%T", test.in), func(t *testing.T) {
			assert.Equal(t, identifier(test.in), test.expected)
		})
	}
}

func TestORMMarshalToMap(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected map[string]interface{}
	}{
		{
			in: User{
				Name:  "test-to-map-user",
				Email: "test-to-map-user@domain.foo",
			},
			expected: map[string]interface{}{
				"name":  "test-to-map-user",
				"email": "test-to-map-user@domain.foo",
			},
		},
		{
			in: User{
				Name:  "test-to-map-user-2",
				Email: "test-to-map-user-2@domain.foo",
			},
			expected: map[string]interface{}{
				"name":  "test-to-map-user-2",
				"email": "test-to-map-user-2@domain.foo",
			},
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-serialise-%T", test.in), func(t *testing.T) {
			assert.Equal(t, test.expected, marshalToMap(test.in))
		})
	}
}

func TestORMReturns(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected string
	}{
		{
			in:       User{},
			expected: "user.name, user.email",
		},
		{
			in:       Team{},
			expected: "team.name",
		},
		{
			in:       Department{},
			expected: "department.name",
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-returns-%T", test.in), func(t *testing.T) {
			actualTokens := strings.Split(returns(test.in), ",")
			expectedTokens := strings.Split(test.expected, ",")
			assert.Equal(t, len(expectedTokens), len(actualTokens))
			assert.EqualValues(t, expectedTokens, actualTokens)
		})
	}
}

func TestORMFilter(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected string
	}{
		{
			in: User{
				Name:  "test-filter-user",
				Email: "test-filter-user@domain.foo",
			},
			expected: `(user.name = "test-filter-user") AND (user.email = "test-filter-user@domain.foo")`,
		},
		{
			in: User{
				Name:  "test-filter-user-2",
				Email: "test-filter-user-2@domain.foo",
			},
			expected: `(user.name = "test-filter-user-2") AND (user.email = "test-filter-user-2@domain.foo")`,
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-filter-%T", test.in), func(t *testing.T) {
			actualTokens := strings.Split(filter(test.in), " AND ")
			expectedTokens := strings.Split(test.expected, " AND ")
			assert.Equal(t, len(expectedTokens), len(actualTokens))
			assert.ElementsMatch(t, expectedTokens, actualTokens)
		})
	}
}

func TestORMFinder(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected string
	}{
		{
			in: User{
				Name:  "test-filter-user",
				Email: "test-filter-user@domain.foo",
			},
			expected: `(user.name = "test-filter-user") OR (user.email = "test-filter-user@domain.foo")`,
		},
		{
			in: User{
				Name:  "test-filter-user-2",
				Email: "test-filter-user-2@domain.foo",
			},
			expected: `(user.name = "test-filter-user-2") OR (user.email = "test-filter-user-2@domain.foo")`,
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-finder-%T", test.in), func(t *testing.T) {
			actualTokens := strings.Split(finder(test.in), " OR ")
			expectedTokens := strings.Split(test.expected, " OR ")
			assert.Equal(t, len(expectedTokens), len(actualTokens))
			assert.ElementsMatch(t, expectedTokens, actualTokens)
		})
	}
}

func TestORMIsZero(t *testing.T) {
	assert.True(t, isZero(User{}))
	assert.True(t, isZero(new(User)))
	assert.True(t, isZero(Team{}))
	assert.True(t, isZero(new(Team)))
	assert.True(t, isZero(Department{}))
	assert.True(t, isZero(new(Department)))
	assert.True(t, isZero(nil))
}

func TestORMEncodingMarshal(t *testing.T) {
	testCases := []struct {
		in       interface{}
		expected string
	}{
		{
			in: User{
				Name:  "test-orm",
				Email: "test-orm@domain.foo",
			},
			expected: `{ name: "test-orm", email: "test-orm@domain.foo" }`,
		},
		{
			in: &User{
				Name:  "test-orm-ptr",
				Email: "test-orm-ptr@domain.foo",
			},
			expected: `{ name: "test-orm-ptr", email: "test-orm-ptr@domain.foo" }`,
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-marshal-%T", test.in), func(t *testing.T) {
			assert.Equal(t, marshal(test.in), test.expected)
		})
	}
}

func TestORMDecodingUnmarshal(t *testing.T) {
	testCases := []struct {
		in       map[string]interface{}
		expected User
	}{
		{
			in: map[string]interface{}{
				"name":  "test-orm-parsed",
				"email": "test-orm-parsed@domain.foo",
			},
			expected: User{
				Name:  "test-orm-parsed",
				Email: "test-orm-parsed@domain.foo",
			},
		},
		{
			in: map[string]interface{}{
				"name":  "test-orm-parsed-ptr",
				"email": "test-orm-parsed-ptr@domain.foo",
			},
			expected: User{
				Name:  "test-orm-parsed-plus",
				Email: "test-orm-parsed-plus@domain.foo",
			},
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("orm-unmarshal-%T", test.expected), func(t *testing.T) {
			var user User
			assert.NoError(t, unmarshal(test.in, &user))
			assert.Equal(t, test.in["name"], user.Name)
			assert.Equal(t, test.in["email"], user.Email)
		})
	}
}
