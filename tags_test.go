package goneo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type taggedUser struct {
	Name     string `goneo:"name,unique"`
	Email    string `goneo:"email,unique,omitempty"`
	Location string
	URI      string `goneo:"-"`
}

type taggedTeam struct {
	Name     string `goneo:"name,unique"`
	Location string
	URI      string `goneo:"-"`
}

type taggedDepartment struct {
	Name     string `goneo:"name,unique"`
	Location string
	URI      string `goneo:"-"`
}

func TestTagsGetRepresentation(t *testing.T) {
	testCases := []struct {
		in                  interface{}
		expectedIdentifiers map[string]reprTag
	}{
		{
			in: new(taggedUser),
			expectedIdentifiers: map[string]reprTag{
				"Name": {
					Identifier: "name",
					Attributes: map[string]bool{
						unique: true,
					},
					Enabled: true,
				},
				"Email": {
					Identifier: "email",
					Attributes: map[string]bool{
						unique:    true,
						omitEmpty: true,
					},
					Enabled: true,
				},
				"Location": {
					Identifier: "Location",
					Enabled:    true,
				},
			},
		},
		{
			in: new(taggedTeam),
			expectedIdentifiers: map[string]reprTag{
				"Name": {
					Identifier: "name",
					Attributes: map[string]bool{
						unique: true,
					},
					Enabled: true,
				},
				"Location": {
					Identifier: "Location",
					Enabled:    true,
				},
			},
		},
		{
			in: new(taggedDepartment),
			expectedIdentifiers: map[string]reprTag{
				"Name": {
					Identifier: "name",
					Attributes: map[string]bool{
						unique: true,
					},
					Enabled: true,
				},
				"Location": {
					Identifier: "Location",
					Enabled:    true,
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("test-tags-GetRepresentation-%T", test.in), func(t *testing.T) {
			repr := getTagsRepresentation(test.in)
			for key, tag := range test.expectedIdentifiers {
				t.Run(key, func(t *testing.T) {
					assert.Equal(t, tag.Enabled, repr[key].Enabled)
					assert.Equal(t, tag.Identifier, repr[key].Identifier)
					if tag.Attributes != nil {
						assert.EqualValues(t, tag.Attributes, repr[key].Attributes)
					}
				})
			}
		})
	}
}
