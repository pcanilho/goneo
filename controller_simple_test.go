package goneo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type ModelTestSuite struct {
	model       interface{}
	expectError bool
}

func setupSimple() {
	user1 = &User{Name: "user-1", Email: "user-1@domain.foo"}
	user2 = &User{Name: "user-2", Email: "user-2@domain.foo"}
	user3 = &User{Name: "user-3"}
	team1 = &Team{Name: "team-1"}
	department1 = &Department{Name: "department-1"}
}

func TestSimpleCreation(t *testing.T) {
	setupSimple()
	suite := []ModelTestSuite{
		{model: user1},
		{model: user2},
		{model: user3},
		{model: team1},
		{model: department1},
		{
			model:       &User{},
			expectError: true,
		},
		{
			model:       &Team{},
			expectError: true,
		},
		{
			model:       &Department{},
			expectError: true,
		},
	}

	for _, e := range suite {
		t.Run(fmt.Sprintf("test-simple-create-%T", e.model), func(t *testing.T) {
			_, err := controllerInstance.Create(e.model)
			if e.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestSimpleListing(t *testing.T) {
	t.Run("test-simple-list-users", func(t *testing.T) {
		var records []User
		err := controllerInstance.List(&records)
		assert.NoError(t, err)
		assert.NotNil(t, records)
		assert.Len(t, records, 3)
	})
	t.Run("test-simple-list-teams", func(t *testing.T) {
		var records []Team
		err := controllerInstance.List(&records)
		assert.NoError(t, err)
		assert.NotNil(t, records)
		assert.Len(t, records, 1)
	})
	t.Run("test-simple-list-department", func(t *testing.T) {
		var records []Department
		err := controllerInstance.List(&records)
		assert.NoError(t, err)
		assert.NotNil(t, records)
		assert.Len(t, records, 1)
	})
}

func TestSimpleLinking(t *testing.T) {
	setupSimple()
	suite := []struct {
		from, to    interface{}
		expectError bool
	}{
		{from: user1, to: team1},
		{from: user2, to: team1},
		{from: user3, to: team1},
		{from: team1, to: department1},
		{from: User{}, to: Team{}, expectError: true},
		{from: new(User), to: new(Team), expectError: true},
		{from: Team{}, to: Department{}, expectError: true},
		{from: new(Team), to: new(Department), expectError: true},
	}

	for _, test := range suite {
		t.Run(fmt.Sprintf("test-simple-%T-to-%T", test.from, test.to), func(t *testing.T) {
			err := controllerInstance.Link(test.from, "Member", test.to)
			if test.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestSimpleFinding(t *testing.T) {
	setupSimple()

	for _, test := range []*User{user1, user2, user3} {
		t.Run(fmt.Sprintf("test-simple-finding-%T", test), func(t *testing.T) {
			var container []User
			err := controllerInstance.Find(test, &container)
			assert.NoError(t, err)
			assert.Len(t, container, 1)
		})
	}
	for _, test := range []*Team{team1} {
		t.Run(fmt.Sprintf("test-simple-finding-%T", test), func(t *testing.T) {
			var container []Team
			err := controllerInstance.Find(test, &container)
			assert.NoError(t, err)
			assert.Len(t, container, 1)
		})
	}
	for _, test := range []*Department{department1} {
		t.Run(fmt.Sprintf("test-simple-finding-%T", test), func(t *testing.T) {
			var container []Department
			err := controllerInstance.Find(test, &container)
			assert.NoError(t, err)
			assert.Len(t, container, 1)
		})
	}
}

func TestSimpleGetLeafs(t *testing.T) {
	setupSimple()
	suite := []struct {
		root          *Team
		expectedLeafs []User
		expectError   bool
	}{
		{root: team1, expectedLeafs: []User{*user1, *user2, *user3}},
		{root: nil, expectedLeafs: []User{*user1, *user2, *user3}, expectError: true},
		{root: nil, expectedLeafs: nil, expectError: true},
	}

	for _, test := range suite {
		t.Run(fmt.Sprintf("test-simple-get-leafs-from-%T", test.root), func(t *testing.T) {
			var records []User
			err := controllerInstance.GetLeafs(test.root, &records, "Member")
			if test.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, records)
				assert.Equal(t, len(test.expectedLeafs), len(records))
				assert.ElementsMatch(t, test.expectedLeafs, records)
			}
		})
	}
}

func TestSimpleDeletion(t *testing.T) {
	setupSimple()
	suite := []struct {
		model       interface{}
		expectError bool
	}{
		{model: user1},
		{model: user2},
		{model: user3},
		{model: User{}, expectError: true},
		{model: Team{}, expectError: true},
	}

	for _, e := range suite {
		t.Run(fmt.Sprint(e), func(t *testing.T) {
			err := controllerInstance.Delete(e.model)
			if e.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
	t.Run("no-users-after-deletion", func(t *testing.T) {
		var records []User
		assert.NoError(t, controllerInstance.List(&records))
		assert.Empty(t, records)
	})

	t.Run("team-persists-after-deletion", func(t *testing.T) {
		var records []Team
		assert.NoError(t, controllerInstance.List(&records))
		assert.NotEmpty(t, records)
		assert.Len(t, records, 1)
	})

	t.Run("department-persists-after-deletion", func(t *testing.T) {
		var records []Department
		assert.NoError(t, controllerInstance.List(&records))
		assert.NotEmpty(t, records)
		assert.Len(t, records, 1)
	})

	t.Run("team-deleted", func(t *testing.T) {
		assert.NoError(t, controllerInstance.Delete(team1))
	})

	t.Run("no-teams-after-deletion", func(t *testing.T) {
		var records []Team
		assert.NoError(t, controllerInstance.List(&records))
		assert.Empty(t, records)
	})

	t.Run("department-deleted", func(t *testing.T) {
		assert.NoError(t, controllerInstance.Delete(department1))
	})

	t.Run("no-teams-after-deletion", func(t *testing.T) {
		var records []Department
		assert.NoError(t, controllerInstance.List(&records))
		assert.Empty(t, records)
	})
}
