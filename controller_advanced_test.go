package goneo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

var queue []interface{}

func setupAdvanced(t *testing.T) {
	user1 = &User{Name: "user-1"}
	user2 = &User{Name: "user-2"}
	user3 = &User{Name: "user-3"}
	user4 = &User{Name: "user-4"}

	team1 = &Team{Name: "team-1"}
	team2 = &Team{Name: "team-2"}
	team3 = &Team{Name: "team-3"}

	department1 = &Department{Name: "department-1"}
	department2 = &Department{Name: "department-2"}
	department3 = &Department{Name: "department-3"}

	queue = []interface{}{
		user1, user2, user3, user4,
		team1, team2, team3,
		department1, department2, department3,
	}

	//Links
	linkMapping := map[interface{}][]interface{}{
		// Users to Teams
		user1: {team1},
		user2: {team1, team2},
		user3: {team2, team3},
		user4: {team3},
		// Teams to Departments
		team1: {department1},
		team2: {department2},
		team3: {department3},
	}

	for _, m := range queue {
		t.Run(fmt.Sprintf("test-advanced-setup-create-model-%T", m), func(t *testing.T) {
			_, err := controllerInstance.Create(m)
			assert.NoError(t, err)
		})
	}

	for src, dsts := range linkMapping {
		for _, dst := range dsts {
			t.Run(fmt.Sprintf("test-advanced-setup-link-model-%T-to-%T", src, dst), func(t *testing.T) {
				assert.NoError(t, controllerInstance.Link(src, "Member", dst))
			})
		}
	}
}

func teardownAdvanced(t *testing.T) {
	for _, m := range queue {
		t.Run(fmt.Sprintf("test-advanced-teardown-delete-model-%T", m), func(t *testing.T) {
			assert.NoError(t, controllerInstance.Delete(m))
		})
	}
}

func TestAdvancedUserPerDepartment(t *testing.T) {
	setupAdvanced(t)
	defer teardownAdvanced(t)

	membershipDepartmentTests := []struct {
		department    *Department
		expectedUsers []User
	}{
		{
			department:    department1,
			expectedUsers: []User{*user1, *user2},
		},
		{
			department:    department2,
			expectedUsers: []User{*user2, *user3},
		},
		{
			department:    department3,
			expectedUsers: []User{*user3, *user4},
		},
	}

	for _, test := range membershipDepartmentTests {
		var records []User
		err := controllerInstance.GetLeafs(test.department, &records, "Member")
		assert.NoError(t, err)
		assert.Len(t, records, len(test.expectedUsers))
		assert.ElementsMatch(t, test.expectedUsers, records)
	}
}
